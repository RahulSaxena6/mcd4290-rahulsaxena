//Task 1
let r = 4;
c = 2*r*Math.PI;
console.log(c.toFixed(2));



//Task 2
let animalString = "cameldogcatlizard";
let andString = " and ";
let catStr = animalString.substr(8,3);
let dogStr = animalString.substr(5,3);
console.log(catStr+andString+dogStr);
let catString = animalString.substring(8,11);
let dogString = animalString.substring(5,8);
console.log(catString+andString+dogString);



//Task 3
let person = {
  'First Name':'Kanye',
  'Last Name':'West',
  'Birth date':'8 June 1977',
  'Annual Income': 150000000.00
}; 

console.log(person['First Name'] + ' was born on ' + person['Birth date'] +
            ' and has an annual income of $' + person['Annual Income']);


//Task 4
var number1, number2;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
//HERE your code to swap the values in number1 and number2
let ex = number1;
number1 = number2;
number2 = ex;
console.log("number1 = " + number1 + " number2 = " + number2);


//Task 5
let year;
let yearNot2015Or2016;
year = 2000;
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);

