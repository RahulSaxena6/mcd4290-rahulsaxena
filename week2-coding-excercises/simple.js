//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    //Question 1 here 
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,-51, -17, -25];
    let PosOdd = [];
    let NegEven = [];
   for(i=0;i<myArray.length;i++){
       if(myArray[i]>0){
           if(myArray[i]%2 !== 0){
                PosOdd += [myArray[i]+" "];}
            } else{
                if(myArray[i]%2 === 0){
                    NegEven += [myArray[i]+" "];}
            }
    }     
    
    output = `Positive Odd: ${PosOdd} Negative Even: ${NegEven}`;
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerHTML= output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    let DieArray = [];
    let Side1 = 0;
    let Side2 = 0;
    let Side3 = 0;
    let Side4 = 0;
    let Side5 = 0;
    let Side6 = 0;

    for(i=0;i<=60000;i++){
          DieArray[i] = (Math.floor((Math.random() * 6) + 1));
            
            if(DieArray[i] === 1){
                    Side1++
            }   else if(DieArray[i] === 2){
                        Side2++
            }       else if(DieArray[i] === 3){
                                Side3++
            }           else if(DieArray[i] === 4){
                                    Side4++
            }               else if(DieArray[i] === 5){
                                        Side5++
            }                   else if(DieArray[i] === 6){
                                            Side6++
            }
            
        }
    
    output = `Frequency of die rolls
            1: ${Side1}                                                                            
            2: ${Side2}                                                                                      
            3: ${Side3}                                                                               
            4: ${Side4}                                                                           
            5: ${Side5}                                                                                     
            6: ${Side6}`


    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "Frequency of die rolls" + "\n";
    
    //Question 3 here 
    
    let Die = [];
    let DieArr = [0,0,0,0,0,0,0];


    for(i=0;i<=60000;i++){
        Die[i] = (Math.floor((Math.random() * 6) + 1));
            let  j = Die[i];
                DieArr[j] += 1
          }
   
    for(j=1;j<DieArr.length;j++) {
        output +=  j + ": " + DieArr[j] + "\n";



    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
    }
}


function question4(){
    let output = "" 
    
    //Question 4 here 

    var dieRolls = {
        Frequencies: {
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
        },
        Total:60000,
        Exceptions: "" 
        }

       for(let i=0;i<dieRolls.Total;i++){
         let dieValue = Math.floor((Math.random() * 6) + 1);
         dieRolls.Frequencies[dieValue]++;
       }


       let onePercent = 10000 * 0.01;
       for(i=0;i<= 6;i++){
            let diff = dieRolls.Frequencies[i] - 10000;
            if(Math.abs(diff) > onePercent) {
                dieRolls.Exceptions++;
            }

            }
        
        
        output = `Frequency of dice rolls
                  Total Rolls:  ${dieRolls.Total}
                  Frequencies: `
        for (let i in dieRolls['Frequencies']){
                output += `${i}: ${dieRolls.Frequencies[i]}`
        }
        
        output += `
        Execptions: ${dieRolls.Exceptions}`


    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    
    let person = {
        name: "Jane",
        income: 127050
        }
    let tax = 600;
    if(person.income >= 0 && person.income <= 18200){
          tax = 0;
    }       else if(person.income >= 18201 && person.income <= 37000){
                  tax = (person.income - 182000)*0.19;
                    }else if(person.income >= 37001 && person.income <= 90000){
                           tax = 3572 + ((person.income - 37000)*0.325);
    }                       else if(person.income >= 90001 && person.income <= 180000){
                                  tax =  20797 + ((person.income - 90000)*0.37);
    }                               else if(person.income >= 180001){
                                          tax =  54097 + ((person.income - 180000)*0.34);
    }                                       
    
    output = `Jane's income is: $${person.income}, and her tax owed is: $${tax.toFixed(2)}`;


    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}