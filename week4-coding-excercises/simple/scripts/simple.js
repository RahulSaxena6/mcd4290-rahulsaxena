function doIt (){
   let Sum =  Number(document.getElementById("number1").value) + Number(document.getElementById("number2").value) + Number(document.getElementById("number3").value);
   let element = document.getElementById("answer");
   element.innerHTML = Sum;
   if(Sum>0){
        element.className = "positive";
   }else{
        element.className = "negative";
   }

   let result = document.getElementById("odd/even");

   if (Sum % 2 === 0){
        result.className = "even";
        result.innerHTML = "(Even)";
   }
   else {
        result.className = "odd";
        result.innerHTML = "(Odd)";
   }
}
