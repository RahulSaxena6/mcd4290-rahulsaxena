// Task 1

var testObj = {
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
    };

let result = "";
const objectToHTML = (obj) => {
    for(let prop in obj){
        result+= `${prop}: ${obj[prop]} 
`
    }
    return result;
} 


document.getElementById("outputArea1").innerText = objectToHTML(testObj);


// Task 2

let outputAreaRef = document.getElementById("outputArea2");

var output = "";
function flexible(fOperation, operand1, operand2) {
let result = fOperation(operand1, operand2);
return result;
}

function Sum(num1, num2) {
    return num1 + num2}

const Multiply = (num1, num2) => {
    return num1*num2;
}


output += flexible(Sum, 3, 5) + "<br/>";
output += flexible(Multiply, 3, 5) + "<br/>";
outputAreaRef.innerHTML = output;



// Task 3
/* 
Declare a function called extremeValues
Create a for loop that compares the first element to every other element in the array
Put the condition that first element should be smaller than every other element in the array
If any element is smaller than first element then that should become the first element
For Maximum just change the condition that the first element should be smaller than all the other elements
Save the values for biggest and smallest in an empty array
Then output the empty array to the HTML
*/


// Task 4
let values = [4, 3, 6, 12, 1, 3, 8];
let smallest = [];
let biggest = [];

const extremeValues = (Arr) => {

    for(let i=0; i<Arr.length; i++){
            for(let j=1; j<Arr.length; j++){
                 if(Arr[i] < Arr[j]){

                   }else{
                        let x = Arr[i] 
                        Arr[i] = Arr[j];
                        Arr[j] = x;
              
            }
          
        }
      
    }
    smallest = Arr[0];

    for(let i=0; i<Arr.length; i++){
        for(let j=1; j<Arr.length; j++){
             if(Arr[i] > Arr[j]){

               }else{
                    let x = Arr[i] 
                    Arr[i] = Arr[j];
                    Arr[j] = x;
          
        }
      
    }
  
}
    biggest = Arr[0];

}


extremeValues(values);

output = `Smallest value in Array: ${smallest} 
          Biggest value in Array: ${biggest}`


document.getElementById("outputArea3").innerText = output;